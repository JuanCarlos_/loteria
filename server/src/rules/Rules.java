package rules;

import board.Board;
import user.Player;
import card.Card;

public class Rules {

	public static boolean validarCarta(Card juego, Card usuario) {
		return juego.equals(usuario);
	}

	public static boolean validarChorro(Board board) {
		return validarDiagonal(board)
				|| validarHorizontal(board)
				|| validarVertical(board);
	}

	private static boolean validarDiagonal(Board board) {
		return (board.get(0, 0).isBeaned() && board.get(1, 1).isBeaned()
				&& board.get(2, 2).isBeaned() && board.get(3, 3).isBeaned())
				|| (board.get(3, 0).isBeaned() && board.get(2, 1).isBeaned()
						&& board.get(1, 2).isBeaned() && board.get(0, 3)
						.isBeaned());

	}

	private static boolean validarHorizontal(Board board) {
		for (int m = 0; m < 4; m++) {
			if (val(board, m, 0) && val(board, m, 1) && val(board, m, 2)
					&& val(board, m, 3)) {
				return true;
			}
		}
		return false;
	}

	private static boolean validarVertical(Board board) {
		for (int n = 0; n < 4; n++) {
			if (val(board, 0, n) && val(board, 1, n) && val(board, 2, n)
					&& val(board, 3, n)) {
				return true;
			}
		}
		return false;
	}

	public static boolean validarEsquinas(Board board) {
		return (val(board, 0, 0) && val(board, 3, 0) && val(board, 0, 3) && val(
				board, 3, 3));
	}

	public static boolean validarCentro(Board board) {
		return (val(board,1,1)&&val(board,2,1)&& val(board,1,2)&& val(board,2,2));
	}

	public static boolean validarLlena(Board board){
		for(Card card : board){
			if(!card.isBeaned()){
				return false;
			}			
		}
		return true;
	}
	
	private static boolean val(Board board, int m, int n) {
		return board.get(m, n).isBeaned();
	}
}
