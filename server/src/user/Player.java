package user;
import board.Board;

public class Player {
	private String name;
	private Board board;
	private boolean chorro;
	private boolean esquinas;
	private boolean centro;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public boolean isChorro() {
		return chorro;
	}

	public void setChorro(boolean chorro) {
		this.chorro = chorro;
	}

	public boolean isEsquinas() {
		return esquinas;
	}

	public void setEsquinas(boolean esquinas) {
		this.esquinas = esquinas;
	}

	public boolean isCentro() {
		return centro;
	}

	public void setCentro(boolean centro) {
		this.centro = centro;
	}

}
