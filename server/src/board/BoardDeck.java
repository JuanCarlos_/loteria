package board;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class BoardDeck extends ArrayList<Board> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 241198282118424362L;

	public BoardDeck() {
		// Se obtienen todas las propiedades definidas en la clase BoardType
		Field fields[] = Boards.class.getDeclaredFields();

		for (int i = 0; i < fields.length; i++) {

			Field field = fields[i];

			// Se extrae la informacion anotada en las propiedades de Boards
			BoardType boardtype = field.getAnnotation(BoardType.class);

			this.add(new Board(boardtype));
		}
	}

	public void shuffle() {
		Collections.shuffle(this, new Random(this.size()));
	}

	public Boolean hasBoard() {
		return !this.isEmpty();
	}

	public void removeBoard(Board board) {
		
		this.remove(board);

	}
	
}
