package card;

public class Cards {
	@CardType(
			id = 1,
			name = "El Gallo"
			)
	private Card _1;
	
	@CardType(
			id = 2,
			name = "El Diablo"
			)
		private Card _2;
		
		@CardType(
			id = 3,
			name = "La Dama"			
			)
		private Card _3;
		
		@CardType(
			id = 4,
			name = "El Catrin"
			)
		private Card _4;
		
		@CardType(
			id = 5,
			name = "El Paraguas"
			)
		private Card _5;
		
		@CardType(
			id = 6,
			name = "La Sirena"
			)
		private Card _6;
		
		@CardType(
			id = 7,
			name = "La Escalera"
			)
		private Card _7;
		
		@CardType(
			id = 8,
			name = "La Botella"
			)
		private Card _8;
		
		@CardType(
			id = 9,
			name = "El Barril"
			)
		private Card _9;
		
		@CardType(
			id = 10,
			name = "El Arbol"
			)
		private Card _10;
		
		@CardType(
			id = 11,
			name = "El Melon"
			)
		private Card _11;
		
		@CardType(
			id = 12,
			name = "El Valiente"
			)
		private Card _12;
		
		@CardType(
			id = 13,
			name = "El Gorro"
			)
		private Card _13;
		
		@CardType(
			id = 14,
			name = "La Muerte"
			)
		private Card _14;
		
		@CardType(
			id = 15,
			name = "La Pera"
			)
		private Card _15;
		
		@CardType(
			id = 16,
			name = "La Bandera"
			)
		private Card _16;
		
		@CardType(
			id = 17,
			name = "El Bandolon"
			)
		private Card _17;
		
		@CardType(
			id = 18,
			name = "El Violoncello"
			)
		private Card _18;
		
		@CardType(
			id = 19,
			name = "La Garza"
			)
		private Card _19;
		
		@CardType(
			id = 20,
			name = "El Pajaro"
			)
		private Card _20;
		
		@CardType(
			id = 21,
			name = "La Mano"
			)
		private Card _21;
		
		@CardType(
			id = 22,
			name = "La Bota"
			)
		private Card _22;
		
		@CardType(
			id = 23,
			name = "La Luna"
			)
		private Card _23;
		
		@CardType(
			id = 24,
			name = "El Cotorro"
			)
		private Card _24;
		
		@CardType(
			id = 25,
			name = "El Borracho"
			)
		private Card _25;
		
		@CardType(
			id = 26,
			name = "El Negrito"
			)
		private Card _26;
		
		@CardType(
			id = 27,
			name = "El Corazon"
			)
		private Card _27;
		
		@CardType(
			id = 28,
			name = "La Sandia"
			)
		private Card _28;
		
		@CardType(
			id = 29,
			name = "El Tambor"
			)
		private Card _29;
		
		@CardType(
			id = 30,
			name = "El Camaron"
			)
		private Card _30;
		
		@CardType(
			id = 31,
			name = "Las Jaras"
			)
		private Card _31;
		
		@CardType(
			id = 32,
			name = "El Musico"
			)
		private Card _32;
		
		@CardType(
			id = 33,
			name = "La Ara�a"
			)
		private Card _33;
		
		@CardType(
			id = 34,
			name = "El Soldado"
			)
		private Card _34;
		
		@CardType(
			id = 35,
			name = "La Estrella"
			)
		private Card _35;
		
		@CardType(
			id = 36,
			name = "El Cazo"
			)
		private Card _36;
		
		@CardType(
			id = 37,
			name = "El Mundo"
			)
		private Card _37;
		
		@CardType(
			id = 38,
			name = "El Apache"
			)
		private Card _38;
		
		@CardType(
			id = 39,
			name = "El Nopal"
			)
		private Card _39;
		
		@CardType(
			id = 40,
			name = "El Alacran"
			)
		private Card _40;
		
		@CardType(
			id = 41,
			name = "La Rosa"
			)
		private Card _41;
		
		@CardType(
			id = 42,
			name = "La Calavera"
			)
		private Card _42;
		
		@CardType(
			id = 43,
			name = "La Campana"
			)
		private Card _43;
		
		@CardType(
			id = 44,
			name = "El Cantaro"
			)
		private Card _44;
		
		@CardType(
			id = 45,
			name = "El Venado"
			)
		private Card _45;
		
		@CardType(
			id = 46,
			name = "El Sol"
			)
		private Card _46;
		
		@CardType(
			id = 47,
			name = "La Corona"
			)
		private Card _47;
		
		@CardType(
			id = 48,
			name = "La Chalupa"
			)
		private Card _48;
		
		@CardType(
			id = 49,
			name = "El Pino"
			)
		private Card _49;
		
		@CardType(
			id = 50,
			name = "El Pescado"
			)
		private Card _50;
		
		@CardType(
			id = 51,
			name = "La Palma"
			)
		private Card _51;
		
		@CardType(
			id = 52,
			name = "La Maceta"
			)
		private Card _52;
		
		@CardType(
			id = 53,
			name = "El Arpa"
			)
		private Card _53;
		
		@CardType(
			id = 54,
			name = "La Rana"
			)
		private Card _54;
}
