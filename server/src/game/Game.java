package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import user.Player;
import board.Board;
import board.BoardDeck;
import card.Card;
import card.Deck;
import rules.Rules;

public class Game extends HashMap<String, Player> {
	private BoardDeck boarddeck;
	private Deck deck;
	private Card current;
	private static final int MAX_PLAYERS = 16;
	private ArrayList<Player> winners, partialwinners;
	private boolean donechorro, doneesquinas, donecentro;

	public Game() {
		deck = new Deck();
		deck.shuffle();

		boarddeck = new BoardDeck();

		winners = new ArrayList<Player>();
		partialwinners = new ArrayList<Player>();

		nextCard();

	}

	/**
	 * @return
	 * @see card.Deck#nextCard()
	 */
	public Card nextCard() {
		finalWinner();
		gotChorro();
		gotEsquinas();
		gotCentro();
		if (winners.isEmpty()) {
			if (!deck.hasCard()) {
				deck = new Deck();
				deck.shuffle();
			}
			current = deck.nextCard();
			return current;
		}
		return null;
	}

	public void addPlayer(String name) {
		if (hasSlots()) {
			Player player = new Player();
			player.setName(name);
			put(name, player);
		}
	}

	public boolean hasSlots() {
		return size() < MAX_PLAYERS;
	}

	public void assignBoard(int id, String name) {
		Board board = boarddeck.get(id - 1);
		get(name).setBoard(board);
		boarddeck.removeBoard(board);
	}

	public void validateCard(int id, String name) {
		/*
		 * for(Card card : get(name).getBoard()){ if(card.getId() == id){
		 * if(Rules.validarCarta(current, card)){ card.setBeaned(true); break; }
		 * } }
		 */
		Board board = get(name).getBoard();
		Card cardtest = new Card(id);
		if (board.contains(cardtest)) {

			int index = board.indexOf(cardtest);
			Card card = board.get(index);
			if (Rules.validarCarta(current, card)) {
				card.setBeaned(true);
			}
		}
	}

	public void finalWinner() {
		for (Player player : values()) {
			if (Rules.validarLlena(player.getBoard())) {
				winners.add(player);
			}
		}
	}

	public void gotChorro() {
		if (!donechorro) {
			for (Player player : values()) {
				if (Rules.validarChorro(player.getBoard())) {
					partialwinners.add(player);
					donechorro = true;
				}
			}
		}
	}

	public void gotEsquinas() {
		if (!doneesquinas) {
			for (Player player : values()) {
				if (Rules.validarEsquinas(player.getBoard())) {
					partialwinners.add(player);
					doneesquinas = true;
				}
			}
		}
	}

	public void gotCentro() {
		if (!donecentro) {
			for (Player player : values()) {
				if (Rules.validarCentro(player.getBoard())) {
					partialwinners.add(player);
					donecentro = true;
				}
			}
		}
	}

	public Card getCurrentCard() {
		return current;
	}

	public List<Board> getBoards() {
		return boarddeck;
	}
}
